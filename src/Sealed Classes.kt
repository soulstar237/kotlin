
sealed class Mammal(val name: String)

class Cat(val catName: String) : Mammal(catName)
class Humen(val humenName: String, val job: String) : Mammal(humenName)

fun greetMammal(mammal: Mammal): String {
    when (mammal) {
        is Humen -> return "Hello ${mammal.name}; You're working"
        is Cat -> return  "Hello ${mammal.name}"
    }
}

fun main() {
    println(greetMammal(Cat("Snowy")))
}