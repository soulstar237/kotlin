//fun main() {
//    var neverNull: String = "This can't be nell"
//    neverNull = null
//
//    var nullable: String? = "You can keep a null here"
//    nullable = null
//
//    var inferredNonNull = "The compiler assumes non-null"
//    inferredNonNull = null
//
//    fun strLenght(notNull: String): Int {
//        return notNull.length
//    }
//    strLenght(neverNull)
//    strLenght(nullable)
//}

// Working with Nulls
fun describeString(maybeString: String?): String {
    if (maybeString != null && maybeString.length > 0) {
        return "String of lenght ${maybeString.length}"
    } else {
        return "Empty or null string"
    }
}

fun main() {
    println(describeString(null))
}