import java.util.*

var table = Array(3) { Array(3) { "-" } }
var turn = 'x'
var arrowPoint: String = "<-"
var rowToInt = 0
var colToInt = 0

fun displayTable() {
    println()
    println("TURN : [X   $arrowPoint   O]")
    println("  1 2 3")
    for (x in table.indices) {
        print("${x + 1} ")
        for (y in table.indices) {
            print(table[x][y] + " ")
        }
        println()
    }
}

fun inputValue() {
    val input = Scanner(System.`in`)
    while (true) {
        try {
            print("Input row and col : ")
            rowToInt = input.nextInt()
            colToInt = input.nextInt()
            if (rowToInt !in 1..3 || colToInt !in 1..3) {
                println("please input two number between 1 - 3 ")
                continue
            }else{
                break
            }
        } catch (e: NumberFormatException) {
            println("Incorrect input please enter number.")
        }
    }
}

fun changeTurn() {
    if (turn == 'x') {
        turn = 'o'
        arrowPoint = "->"
    } else {
        turn = 'x'
        arrowPoint = "<-"
    }
}

fun checkPosition() {
    if (table[rowToInt - 1][colToInt - 1] == "-") {
        table[rowToInt - 1][colToInt - 1] = turn.toString()
    } else if (table[rowToInt - 1][colToInt - 1] != "-") {
        changeTurn()
        println("This Position is already selected!!")
    }
}

fun isWin(): Boolean {
    for (i in 1..8) {
        var line: String? = null
        when (i) {
            1 -> line = (table[0][0] + table[0][1] + table[0][2]).toString()
            2 -> line = (table[1][0] + table[1][1] + table[1][2]).toString()
            3 -> line = (table[2][0] + table[2][1] + table[2][2]).toString()
            4 -> line = (table[0][0] + table[1][0] + table[2][0]).toString()
            5 -> line = (table[0][1] + table[1][1] + table[2][1]).toString()
            6 -> line = (table[0][2] + table[1][2] + table[2][2]).toString()
            7 -> line = (table[0][0] + table[1][1] + table[2][2]).toString()
            8 -> line = (table[0][2] + table[1][1] + table[2][0]).toString()
        }
        if (line == "xxx") {
            return true
        } else if (line == "ooo") {
            return true
        }
    }
    return false
}


fun displayWinner() {
    println("$turn is a winner !!!")
}

fun main() {
    while (true) {
        displayTable()
        inputValue()
        checkPosition()
        if (isWin()) {
            displayTable()
            displayWinner()
            break
        }
        changeTurn()
    }
}