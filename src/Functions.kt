
fun printMessage(message : String) : Unit {
    println(message)
}

fun printMessageWithPrefix(message: String, prefix: String = "Info") {
    println("[$prefix] $message")
}

fun sum(x: Int, y: Int): Int {
    return x + y
}

fun multiply(x: Int, y: Int) = x * y

//fun main() {
//    printMessage("Hello")
//    printMessageWithPrefix("Hello", "Log")
//    printMessageWithPrefix("Hello")
//    printMessageWithPrefix(prefix = "Log", message = "Hello")
//    println(sum(1,2))
//}


fun main() {
//    operator fun Int.times(str: String) = str.repeat(this)
//    println(2 * "Bye ")
//
//    val pair = "Ferrari" to "Katrina"
//    println(pair)
//
//    infix fun String.onto(other : String) = Pair(this, other)
//    val myPair = "McLaren" onto "Lucas"
//    println(myPair)
//
//    val sophia = Person("Sophia")
//    val claudia = Person("claudia")
//    sophia likes claudia
//
//    operator fun String.get(range: IntRange) = substring(range)
//    val str = "Always forgive your enemies; nothing annoys them so much."
//    println(str[0..14])
    printAll("Hello", "Hallo", "Salut", "Hola", "你好")
    printAllWithPrefix(
            "Hello", "Hallo", "Salut", "Hola", "你好",
            prefix = "Greeting: "
    )
}

class Person(val name: String) {
    val linkedPerson = mutableListOf<Person>()
    infix fun likes(other: Person) { linkedPerson.add(other)}
}

fun printAll(vararg message: String){
    for (m in message) println(m)
}

fun printAllWithPrefix(vararg message: String, prefix: String) {
    for (m in message) println(prefix + m)
}

fun log(vararg entries : String) {
    printAll(* entries)
}